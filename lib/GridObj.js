export default class GridObj {
  constructor(grid) {
    this.grid = helpers.deepCopy(grid)
    this.liters = 0
  }

  clean() {
    Object.keys(this.grid).forEach(n => {
      this.grid[n].row.forEach(cell => {
        if (cell.val === 2) cell.val = 0
      })
    })
  }

  handleClick(_cell) {
    const cell = this.grid[_cell.n].row[_cell.i]
    const { val } = cell

    if (val === 0) {
      this.handleWhiteCell(cell)
    } else if (val === 1) {
      this.handleBlackCell(cell)
    }
  }

  handleWhiteCell(cell) {
    const { n, i } = cell
    if (n === 0 || this.grid[n - 1].row[i].val === 1) {
      cell.val = 1
    }
  }

  handleBlackCell(cell) {
    const { n, i } = cell
    const total = Object.keys(this.grid).length
    const last = total - 1

    if (n === last || this.grid[n + 1].row[i].val === 0) {
      cell.val = 0
    }
  }

  fill() {
    const arr = helpers.convertGridFromState(this.grid)
    const max = Math.max(...arr.map(item => item.bricks), 0)
    if (max === 0) return
    const maxIndexes = []

    arr.forEach((item, i) => {
      if (item.bricks === max) maxIndexes.push(i)
    })

    const rightMax = maxIndexes[maxIndexes.length - 1]
    this.fillLeft(arr, rightMax)
    this.fillRight(arr, rightMax)
  }

  fillLeft(arr, index) {
    let max = arr[0].bricks
    for (let i = 0; i < index; i++) {
      const next = arr[i + 1]
      if (max > next.bricks) {
        next.water = max - next.bricks
        this.fillWithWater(i + 1, next)
      } else {
        max = next.bricks
      }
    }
  }

  fillRight(arr, index) {
    const last = arr.length - 1
    let max = arr[last].bricks
    for (let i = last; i > index; i--) {
      const next = arr[i - 1]
      if (max > next.bricks) {
        next.water = max - next.bricks
        this.fillWithWater(i - 1, next)
      } else {
        max = next.bricks
      }
    }
  }

  fillWithWater(index, item) {
    let i = item.water
    this.liters += i
    while (i > 0) {
      const n = item.bricks + i - 1
      this.grid[n].row[index].val = 2
      i--
    }
  }
}
