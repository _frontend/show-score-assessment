import * as path from 'path'

const rootPath = path.join(__dirname, '..')
const base = (...args) => Reflect.apply(path.resolve, null, [rootPath, ...args])

export default {
  default: {
    paths: base,
    srcPath: base('src'),
    distPath: base('dist'),
    rootPath,
    publicPath: '/',
    compilerHashType: 'hash',
    compilerDevtool: 'cheap-module-source-map',
    serverPort: 3000,
  },
  development: {},
  production: {
    publicPath: './',
    compilerHashType: 'chunkhash',
    compilerDevtool: false,
  }
}
