#!/bin/bash

yarn compile
shopt -s extglob
ln -s ./public/!(*favicon.ico) ./dist --relative
