### Install

node 10.6.0

```
$ yarn install
```

### Development

`$ yarn start`

open localhost:3000

### Production
`$ yarn build`

open dist/index.html file in browser
