export const deepCopy = (o) => {
  let i
  let newO

  if (typeof o !== 'object') return o
  if (!o) return o

  if (Object.prototype.toString.apply(o) === '[object Array]') {
    newO = []
    for (i = 0; i < o.length; i += 1) {
      newO[i] = deepCopy(o[i])
    }
    return newO
  }

  newO = {}
  for (i in o) {
    if (o.hasOwnProperty(i)) newO[i] = deepCopy(o[i])
  }
  return newO
}

export const convertGridFromState = (grid) => {
  const arr = []
  const result = []
  const total = Object.keys(grid).length

  Object.keys(grid).forEach(n => {
    grid[n].row.forEach(cell => {
      const { n, i, val } = cell
      arr[i] = (arr[i] || [])
      arr[i][n] = val
    })
  })

  arr.forEach((column, i) => {
    const counts = {}
    column.forEach(item => {
      counts[item] = (counts[item] || 0) + 1
    })
    result[i] = { total, bricks: counts[1] || 0, water: counts[2] || 0 }
  })

  return result
}
