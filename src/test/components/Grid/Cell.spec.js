import React from 'react'
import { bindActionCreators } from 'redux'
import { shallow } from 'enzyme'
import Cell from '../../../components/Grid/Cell'
import store from '../../../store'
import * as GridActions from '../../../reducers/grid'

const actions = bindActionCreators(GridActions, store.dispatch)
const cell = { i: 0, n: 0, val: 0 }

it('simulates click events', () => {
  const wrapper = shallow(<Cell actions={actions} cell={cell} />)
  wrapper.find('div').simulate('click')
  expect(store.getState().grid[0].row[0].val).toBe(1)
})
