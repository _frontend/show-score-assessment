import renderer from 'react-test-renderer'
import App from '../../../components/App/App'
import store from '../../../store'

it('renders correctly', () => {
  const tree = renderer
    .create(<App store={store} />)
    .toJSON()
  expect(tree).toMatchSnapshot()
})
