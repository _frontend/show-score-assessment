import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'
import * as helpers from '../../helpers'

configure({ adapter: new Adapter() })
global.helpers = helpers
global.React = React
