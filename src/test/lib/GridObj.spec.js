import GridObj from '../../../lib/GridObj'

// grid 6x4
const grid = {}
for (let n = 0; n < 4; n++) {
  grid[n] = { row: [] }
  for (let i = 0; i < 6; i++) {
    grid[n].row.push({ i, n, val: 0 })
  }
}

const gridObj = new GridObj(grid)

beforeEach(() => {
  gridObj.grid = grid
})

describe('GridObj', () => {
  it('should remove water cells', () => {
    grid[0].row[0].val = 2
    grid[1].row[2].val = 2

    gridObj.clean()

    expect(gridObj.grid[0].row[0].val).toBe(0)
    expect(gridObj.grid[1].row[2].val).toBe(0)
  })

  it('should not handle click for water cell', () => {
    const spy1 = jest.spyOn(gridObj, 'handleWhiteCell')
    const spy2 = jest.spyOn(gridObj, 'handleBlackCell')
    gridObj.grid[0].row[0].val = 2

    gridObj.handleClick({ n: 0, i: 0 })

    expect(spy1).not.toHaveBeenCalled()
    expect(spy2).not.toHaveBeenCalled()
  })

  it('should handle click for black & white cells', () => {
    const spy1 = jest.spyOn(gridObj, 'handleWhiteCell')
    const spy2 = jest.spyOn(gridObj, 'handleBlackCell')

    gridObj.grid[0].row[0].val = 1
    gridObj.handleClick({ n: 0, i: 0 })
    expect(spy2).toHaveBeenCalled()

    gridObj.grid[0].row[0].val = 0
    gridObj.handleClick({ n: 0, i: 0 })
    expect(spy1).toHaveBeenCalled()
  })

  it('should fill cells with water', () => {
    // # #
    // # # #
    gridObj.grid[0].row[0].val = 1
    gridObj.grid[0].row[2].val = 1
    gridObj.grid[0].row[4].val = 1
    gridObj.grid[1].row[0].val = 1
    gridObj.grid[1].row[2].val = 1
    gridObj.fill()
    expect(gridObj.liters).toBe(3)
  })
})

// TODO: add more test
