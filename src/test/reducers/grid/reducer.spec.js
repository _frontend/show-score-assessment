import * as Grid from '../../../reducers/grid'

describe('Grid Reducer', () => {
  it('should create an action to add a todo', () => {
    const payload = { key: 'val' }
    const expectedAction = {
      type: Grid.CHANGE,
      payload
    }
    expect(Grid.change({ key: 'val' })).toEqual(expectedAction)
  })
})
