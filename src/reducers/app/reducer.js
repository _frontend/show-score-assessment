export const UPDATE = 'APP:UPDATE'

const initialState = {
  liters: 0,
}

export default function (state = initialState, { type, payload }) {
  switch (type) {
    case UPDATE:
      return { ...state, ...payload }
    default:
      return state
  }
}

export const update = payload => ({ type: UPDATE, payload })
