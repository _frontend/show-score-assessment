import GridObj from '../../../lib/GridObj'

export const CHANGE = 'GRID:CHANGE'
export const CLEAN = 'GRID:CLEAN'
export const FILL = 'GRID:FILL'
export const RESET = 'GRID:RESET'

const initialState = {}

// creating grid obj
for (let n = 0; n < 6; n++) {
  initialState[n] = { row: [] }
  for (let i = 0; i < 12; i++) {
    initialState[n].row.push({ i, n, val: 0 })
  }
}

export default function (state = initialState, { type, payload }) {
  switch (type) {
    case CHANGE:
    case CLEAN:
      return payload
    case FILL:
      return payload.grid
    case RESET:
      return initialState
    default:
      return state
  }
}

export const change = payload => ({ type: CHANGE, payload })

export const clean = payload => ({ type: CLEAN, payload })

export const fill = payload => ({ type: FILL, payload })

export const reset = () => ({ type: RESET })

export const click = payload => (dispatch, getState) => {
  const state = getState()
  const { app, grid } = state
  const gridObj = new GridObj(grid, dispatch)

  // ho handle on water cell click
  if (payload.val === 2) return

  // handle click while filled in with water
  if (app.liters > 0) {
    gridObj.clean()
    dispatch(clean(gridObj.grid))
    return
  }

  // handle black & white cell click
  gridObj.handleClick(payload)
  dispatch(change(gridObj.grid))
}

export const run = () => (dispatch, getState) => {
  const state = getState()
  const { grid } = state
  const gridObj = new GridObj(grid, dispatch)

  gridObj.fill()
  dispatch(fill(gridObj))
}
