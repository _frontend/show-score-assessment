import { object } from 'prop-types'
import Row from './Row'
import css from './Grid.scss'

const Grid = ({ actions, grid }) => {
  const reset = () => actions.reset()
  const run = () => actions.run()

  return (
    <div className={css.gridHolder}>
      <div className={css.grid}>
        <table>
          <tbody>
            {Object.keys(grid).sort().reverse().map(n => <Row key={n} row={grid[n].row} actions={actions} />)}
          </tbody>
        </table>
      </div>
      <div className={css.controls}>
        <button type="button" onClick={reset}>reset</button>
        <button type="button" onClick={run} className={css.active}>run</button>
      </div>
    </div>
  )
}

Grid.propTypes = {
  actions: object.isRequired,
  grid: object.isRequired,
}

export default Grid
