import { array, object } from 'prop-types'
import Cell from './Cell'

const Row = ({ row, actions }) => (
  <tr>
    {row.map((cell) => <Cell key={`${cell.n}${cell.i}`} cell={cell} actions={actions} />)}
  </tr>
)

Row.propTypes = {
  actions: object.isRequired,
  row: array.isRequired,
}

export default Row
