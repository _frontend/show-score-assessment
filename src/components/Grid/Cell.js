import { object } from 'prop-types'
import classnames from 'classnames'
import css from './Grid.scss'

export default class Cell extends React.Component {
  shouldComponentUpdate(nextProps) {
    const { cell } = this.props
    return cell.val !== nextProps.cell.val
  }

  onClick = () => {
    const { actions, cell } = this.props
    actions.click(cell)
  }

  render() {
    const { cell } = this.props
    const { val } = cell
    const divProps = {
      onClick: this.onClick,
      className: classnames({
        [css.brick]: val === 1,
        [css.water]: val === 2
      })
    }
    return (
      <td>
        <div {...divProps} />
      </td>
    )
  }
}

Cell.propTypes = {
  actions: object.isRequired,
  cell: object.isRequired,
}
