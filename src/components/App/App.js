import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { object } from 'prop-types'
import * as GridActions from '../../reducers/grid'
import Grid from '../Grid'
import css from './App.scss'

const App = ({ grid, gridActions }) => (
  <div className={css.pageHolder}>
    <div className={css.header}>
      <h1><span>show-score</span><i>&#8480;</i> assessment</h1>
    </div>
    <Grid grid={grid} actions={gridActions} />
  </div>
)

App.propTypes = {
  grid: object.isRequired,
}

const mapStateToProps = state => ({
  grid: state.grid,
})

const mapDispatchToProps = dispatch => ({
  gridActions: bindActionCreators(GridActions, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(App)
