/* eslint default-case: 0 */

import * as App from '../reducers/app'
import * as Grid from '../reducers/grid'

export default ({ dispatch }) => next => action => {
  const { type, payload } = action

  switch (type) {
    case Grid.FILL: {
      const { liters } = payload
      dispatch(App.update({ liters }))
      break
    }
    case Grid.CLEAN:
    case Grid.RESET:
      dispatch(App.update({ liters: 0 }))
      break
  }

  next(action)
}
