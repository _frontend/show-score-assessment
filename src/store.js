import { applyMiddleware, createStore, compose } from 'redux'
import thunk from 'redux-thunk'
import appMiddleware from './middleware/app'
import reducers from './reducers'

const store = createStore(reducers, compose(applyMiddleware(thunk, appMiddleware)))

export default store
