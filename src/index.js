import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import App from './containers/App'
import '../vendor/stylesheets/style.scss'
import '../vendor/stylesheets/fonts.css'

const MOUNT_NODE = document.getElementById('root')
const layout = <Provider store={store}><App /></Provider>

if (MOUNT_NODE) ReactDOM.render(layout, MOUNT_NODE)
